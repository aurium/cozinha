"use strict";

const q = document.querySelector.bind(document);
const qAll = document.querySelectorAll.bind(document);
const tables = {};

function qEach(query, fn) {
  let list = qAll(query);
  for ( let el,i=0; el=list[i]; i++ ) fn(el);
}

function mkEl(tag, opts) {
  let el = document.createElement(tag);
  for (let att in opts)
    if (att != 'parent' && att != 'txt' && att != 'html') el[att] = opts[att];
  if (opts.parent) opts.parent.appendChild(el);
  if (opts.txt) el.innerText = opts.txt;
  if (opts.html) el.innerHTML = opts.html;
  return el;
}

function notNull(value) { return value!=null && typeof(value)!='undefined' }

function val(el) {
  if (typeof(el)=='string') el = q(el);
  if (!el) return null;
  if (notNull(el.val)) return el.val;
  let input = (el.tagName=='INPUT') ? el : el.querySelector('input');
  if (notNull(input.val)) return input.val;
  return calc(input);
}

function round2(num) {
  if (typeof(num)=='string') return num;
  let r = (Math.round(num*100)/100).toString();
  /\./.test(r) || (r += '.');
  while (!/\.../.test(r)) r += '0';
  return r;
}

function calc(input) {
  if (!input.value || !input.value.length) {
    throw Error(`invalid input: "${input.outerHTML}"`);
  }
  let value, valEl;
  if (valEl = input.parentNode.querySelector('code'))
    input.parentNode.removeChild(valEl);
  if (/^[.0-9]+$/.test(input.value)) {
    value = parseFloat(input.value);
  } else {
    let mathExp = input.value
                       .replace(/"/g, '')
                       .replace(/([a-z][a-z0-9_]+)/ig, 'val("#$1")');
    try {
      value = eval(mathExp);
    } catch(err) {
      value = '😡';
      console.log(input.value +'\n'+ mathExp, err)
    }
    if (!value) value = 0;
    mkEl('code', { txt: round2(value), parent: input.parentNode });
  }
  input.val = value;
  input.size = input.value.length || 1;
  return value;
}

tables.materialMes = {
  update: ()=> qEach('#tabMaterialMensal input', (input)=> calc(input))
};

function custoMaterialDiaProd() {
  let tot = 0;
  qEach('#tabMaterialMensal input', (input)=> tot += val(input)/val(diasProdMes));
  return tot;
}

tables.maoDeObra = {
  update: ()=> qEach('#tabMaoDeObra input.diaria', (input)=> {
    let diaria = calc(input);
    let tdValMes = input.parentNode.parentNode.querySelector('.mao-valMes');
    let tdValDia = input.parentNode.parentNode.querySelector('.mao-valDia');
    let diasPorSemana = parseInt(input.parentNode.parentNode.querySelector('.dps').value);
    tdValMes.val = diaria * diasPorSemana * (30/7);
    tdValMes.innerText = round2(tdValMes.val);
    tdValDia.val = tdValMes.val / val(diasProdMes);
    tdValDia.innerText = round2(tdValDia.val);
  })
};

function custoMaoDeObraDiaProd() {
  let tot = 0;
  qEach('#tabMaoDeObra .mao-valDia', (td)=> tot += val(td));
  return tot;
}

tables.resumoCustoFixo = {
  update: ()=> {
    let tdValDia, tdValMes;
    tdValDia = q('#resumoCustoMaterial .valDia');
    tdValMes = q('#resumoCustoMaterial .valMes');
    tdValDia.val = custoMaterialDiaProd();
    tdValDia.innerText = round2(tdValDia.val);
    tdValMes.val = tdValDia.val * val(diasProdMes);
    tdValMes.innerText = round2(tdValMes.val);

    tdValDia = q('#resumoCustoMaoDeObra .valDia');
    tdValMes = q('#resumoCustoMaoDeObra .valMes');
    tdValDia.val = custoMaoDeObraDiaProd();
    tdValDia.innerText = round2(tdValDia.val);
    tdValMes.val = tdValDia.val * val(diasProdMes);
    tdValMes.innerText = round2(tdValMes.val);

    tdValDia = q('#resumoTotal .valDia');
    tdValMes = q('#resumoTotal .valMes');
    tdValDia.val = custoDiaProd();
    tdValDia.innerText = round2(tdValDia.val);
    tdValMes.val = tdValDia.val * val(diasProdMes);
    tdValMes.innerText = round2(tdValMes.val);
  }
};

function custoDiaProd() {
  return custoMaterialDiaProd() + custoMaoDeObraDiaProd();
}

function calcProd(idProd, listUnitProdDia) {
  let table = [];
  let valComida = val('#'+idProd+' .valComida');
  let lucro = val('#'+idProd+' .lucro');
  listUnitProdDia.forEach((unitProdDia)=> {
    let line = {};
    line.unitProdDia = unitProdDia
    table.push(line);
  });
  return table;
}

function showCalcProd(idProd, listUnitProdDia) {
  let table = calcProd(idProd, listUnitProdDia);
  let header = '<th>Produção Dia</th> <th>Val Base</th>';
  let tabela = q('#'+idProd+' table');
  while ( tabela.firstChild ) tabela.removeChild(tabela.firstChild);
  header += '<th>Freezer Usado</th><th>Val c/Lucro<br><small>(arredondado)</small></th>';
  mkEl('tr', { html: header, parent: tabela });
  table.forEach((line)=> {
    let tr = mkEl('tr', { parent: tabela });
    mkEl('td', { html: 'R$ '+round2(line.valBasePac)+' &nbsp;', align:'right', parent: tr });
    mkEl('td', { txt: 'R$ '+round2(line.valLPac), align:'right', parent: tr });
  });
}

tables.prod01 = {
  update: ()=> showCalcProd('prod-01', [40,60,80,100,120,150,200])
}

try {
  function updateAll() {
    for (name in tables) tables[name].update();
  }
  updateAll();

  let varlist = [];
  qEach('input:not([type="text"])', (input)=>{
    calc(input);
    input.addEventListener('keyup', ()=> calc(input));
    input.addEventListener('change', updateAll);
    input.addEventListener('focus', ()=> q('#varlist-box').style.display='block');
    input.addEventListener('blur', ()=> q('#varlist-box').style.display='none');
    let nameRef = input.id || input.parentNode.id;
    if (nameRef) {
      input.title = `Referência: ${nameRef}`;
      varlist.push(`<b>${nameRef}</b>`);
    } else {
      input.title = `Sem referência.`;
    }
  });
  q('#varlist').innerHTML = varlist.join('');

  qEach('select', (input)=>{
    input.onchange = updateAll;
  });
} catch(err) {
  console.log('Shit happens... '+err.message, err.stack);
  alert('Ups! '+err.message);
}
